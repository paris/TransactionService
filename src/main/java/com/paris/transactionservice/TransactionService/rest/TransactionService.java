package com.paris.transactionservice.TransactionService.rest;

import com.paris.transactionservice.TransactionService.transaction.Transaction;
import com.paris.transactionservice.TransactionService.transaction.TransactionManager;

import javax.ws.rs.*;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;
import java.util.ArrayList;

@Path("/transactions")
public class TransactionService extends Application {

    TransactionManager tm = new TransactionManager();

    @GET
    public ArrayList<Transaction> getTransactions(){

        ArrayList<Transaction> allTransactions = tm.getAllTransactions();
        return allTransactions;

    }

    @Path("/{accountID}")
    @GET
    public ArrayList<Transaction> getTransactionsByAccount(@PathParam(value = "accountID") String accountID){

        //If it's a merchant, it shouldn't include customer IDs
        ArrayList<Transaction> accountTransactions = tm.getTransactionsByAccount(accountID);
        return accountTransactions;

    }

    @POST
    public Response createTransaction(@QueryParam("merchantID") String merchantID, @QueryParam("tokenID")
            String tokenID, @QueryParam("customerID") String customerID, @QueryParam("amount") double amount){

        Transaction t = tm.createTransaction(merchantID,tokenID,customerID,amount);
        //if(tm.createTransaction(merchantID,tokenID,customerID,amount)){
        //    return Response.ok().build();
        //}

        if(t != null){
            return Response.ok(t).build();
        }
        System.out.println("Failed.");

        return Response.notModified().build();

    }

}
