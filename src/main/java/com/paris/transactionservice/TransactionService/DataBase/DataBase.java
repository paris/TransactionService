package com.paris.transactionservice.TransactionService.DataBase;

import com.paris.transactionservice.TransactionService.transaction.Transaction;

import java.util.ArrayList;

public class DataBase {

    ArrayList<Transaction> transactions = new ArrayList<>();

    public void addTransaction(Transaction transaction) {
        transactions.add(transaction);
    }

    public ArrayList<Transaction> getAllTransactions() {
        return transactions;
    }

    public ArrayList<Transaction> getCustomerTransactions(String custID){
        ArrayList<Transaction> customerTransactions = new ArrayList<>();
        for (int i = 0; i < transactions.size() ; i++){
            Transaction tr = transactions.get(i);
            if(tr.getCustomerID().equals(custID)){
                customerTransactions.add(tr);
            }
        }
        return customerTransactions;
    }
}
