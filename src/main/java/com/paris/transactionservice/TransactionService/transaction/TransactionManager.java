package com.paris.transactionservice.TransactionService.transaction;

import com.paris.transactionservice.TransactionService.DataBase.DataBase;
import com.paris.transactionservice.TransactionService.MessengerService.MessengerService;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.UUID;

public class TransactionManager {
    private DataBase db = new DataBase();
    private MessengerService ms = new MessengerService();

    public Transaction createTransaction(String merchantID, String tokenID, String customerID, double amount){

        if(!checkMerchantID(merchantID)){
            return null;
        }
        if(!checkToken(tokenID)){
            return null;
        }
        if(!checkCustomer(customerID)){
            return null;
        }
        if(!processTransaction(merchantID, tokenID, customerID, amount)){
            return null;
        }
        Transaction transaction = new Transaction(generateTransactionID(), merchantID, tokenID, customerID, amount);
        db.addTransaction(transaction);
        return transaction;
    }

    private UUID generateTransactionID() {
        UUID id = UUID.randomUUID();
        return id;
    }

    private boolean processTransaction(String merchantID, String tokenID, String customerID, double amount) {
        return ms.createTransaction();
    }

    private boolean checkMerchantID(String merchantID) {
        return ms.checkMerchant(merchantID);
    }

    private boolean checkToken(String tokenID) {
        return ms.checkToken(tokenID);
    }

    private boolean checkCustomer(String customerID) { return ms.checkCustomer(customerID); }


    public ArrayList<Transaction> getAllTransactions() {

        //Just for testing, erase this later
        createTransaction("MerchID","TokID","CustID",100);
        createTransaction("MerchID2","TokID2","CustID2",200);
        createTransaction("MerchID3","TokID3","CustID3",300);
        //End testing stuff

        ArrayList<Transaction> allTransactions = db.getAllTransactions();
        return allTransactions;
    }

    public ArrayList<Transaction> getTransactionsByAccount(String accountID){

        //If it's a merchant, it shouldn't include customer IDs
        ArrayList<Transaction> allTransactions = db.getAllTransactions();
        ArrayList<Transaction> customerTransactions = new ArrayList<>();
        for (int i=0; i < allTransactions.size(); i++){
            Transaction t = allTransactions.get(i);
            if(t.getCustomerID().equals(accountID)){
                customerTransactions.add(t);
            }
        }
        return customerTransactions;
    }

    /*public ArrayList<Transaction> getTransactionsByMerchant(String merchantID){
        ArrayList<Transaction> allTransactions = db.getAllTransactions();
        ArrayList<Transaction> customerTransactions = new ArrayList<>();
        for (int i=0; i < allTransactions.size(); i++){
            Transaction t = allTransactions.get(i);
            if(t.getCustomerID().equals(merchantID)){
                customerTransactions.add(t);
            }
        }
        return customerTransactions;
    }*/
}
