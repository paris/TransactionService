package com.paris.transactionservice.TransactionService.transaction;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.UUID;

@XmlRootElement(name="Transaction")
public class Transaction {
    private UUID transactionID;
    private String merchantID;
    private String tokenID;
    private String customerID;
    private double amount;

    public Transaction(){}

    public Transaction(UUID transactionID, String merchantID, String tokenID, String customerID, double amount) {
        this.transactionID = transactionID;
        this.merchantID = merchantID;
        this.tokenID = tokenID;
        this.customerID = customerID;
        this.amount = amount;
    }

    public UUID getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(UUID transactionID) {
        this.transactionID = transactionID;
    }

    public String getMerchantID() {
        return merchantID;
    }

    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    public String getTokenID() {
        return tokenID;
    }

    public void setTokenID(String tokenID) {
        this.tokenID = tokenID;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }
}
